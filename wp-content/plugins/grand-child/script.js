/*store location start*/

// header flyer choose location js start here 
jQuery(document).on('click', '.choose_location', function() {
    //alert('mystore');
    var mystore = jQuery(this).attr("data-id");
    // alert('mystore');
    var distance = jQuery(this).attr("data-distance");

    jQuery.cookie("greatfloors_store", null, { path: '/' });
    jQuery.cookie("greatfloors_distance", null, { path: '/' });

    jQuery.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        data: 'action=choose_location&store_id=' + jQuery(this).attr("data-id") + '&distance=' + jQuery(this).attr("data-distance"),

        success: function(data) {

            jQuery(".header_store").html(data);
            jQuery.cookie("greatfloors_store", mystore, { expires: 1, path: '/' });
            jQuery.cookie("greatfloors_distance", distance, { expires: 1, path: '/' });
            setTimeout(addFlyerEvent, 1000);

        }
    });

});

// flyer close function 
function closeNav() {
    let storeLocation = document.getElementById("storeLocation");
    storeLocation.style.right = "-100%";
    storeLocation.classList.remove("ForOverlay");
}

// flyer event assign function
function addFlyerEvent() {
    document.getElementById('openFlyer').addEventListener("click", function(e) {
        let storeLocation = document.getElementById("storeLocation");
        storeLocation.style.right = "0";
        storeLocation.classList.add("ForOverlay");
    });
}

jQuery(document).ready(function() {

    jQuery(function($) {
        $('#storeLocation .choose_location').on('click', closeNav);

    });

    var mystore = jQuery.cookie("greatfloors_store");
    var distance = jQuery.cookie("greatfloors_distance");

    jQuery.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        data: 'action=get_storelisting',

        success: function(data) {

            jQuery(".header_store").html(data);
            setTimeout(addFlyerEvent, 1000);
            var mystore_loc = jQuery(".contentFlyer h3").html();
            jQuery("#input_22_13").val(mystore_loc);
            jQuery("#input_24_12").val(mystore_loc);
            jQuery("#input_30_12").val(mystore_loc);
            jQuery("#input_16_9").val(mystore_loc);
            jQuery("#input_19_16").val(mystore_loc);
        }
    });

    jQuery.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        data: 'action=choose_location_listing',

        success: function(data) {

            jQuery("#ajaxstorelisting").html(data);
            setTimeout(addFlyerEvent, 1000);
            $('#storeLocation .choose_location').on('click', closeNav);
            var mystore_loc = jQuery(".contentFlyer h3").html();
            jQuery("#input_22_13").val(mystore_loc);
            jQuery("#input_24_12").val(mystore_loc);
            jQuery("#input_30_12").val(mystore_loc);
            jQuery("#input_16_9").val(mystore_loc);
            jQuery("#input_19_16").val(mystore_loc);
        }
    });

    jQuery.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        data: 'action=choose_location_listing',

        success: function(data) {

            jQuery("#ajaxstorelisting").html(data);
            setTimeout(addFlyerEvent, 1000);
            $('#storeLocation .choose_location').on('click', closeNav);
            var mystore_loc = jQuery(".contentFlyer h3").html();
            // alert(mystore_loc);

            jQuery("#input_22_13").val(mystore_loc);
            jQuery("#input_24_12").val(mystore_loc);
            jQuery("#input_30_12").val(mystore_loc);
            jQuery("#input_16_9").val(mystore_loc);
            jQuery("#input_19_16").val(mystore_loc);
        }
    });

    /*store location end*/

});

function addFavProduct(is_fav, sku, prod_id) {

    var queryString = "";
    queryString = 'action=add_fav_product&sku=' + sku + '&post_id=' + prod_id + '&is_fav=' + is_fav;
    jQuery.ajax({
        url: "/wp-admin/admin-ajax.php",
        data: queryString,
        type: "POST",
        dataType: "json",
        success: function(response) {

            if (is_fav == 0) {
                is_fav1 = 1;
            } else {
                is_fav1 = 0;
            }

            $(".is_fav" + sku).attr("onClick", "addFavProduct('" + is_fav1 + "','" + sku + "','" + prod_id + "')");
            $(".is_fav" + sku).removeClass("is_fav0");
            $(".is_fav" + sku).removeClass("is_fav1");
            $(".is_fav" + sku).toggleClass("is_fav" + is_fav1);
        },
        error: function() {}
    });
}

function printDiv(divName) {
    var printContents = document.getElementById("printMe").innerHTML;
    var originalContents = document.body.innerHTML;

    document.body.innerHTML = printContents;

    window.print();

    document.body.innerHTML = originalContents;

}

jQuery(document).on('click', '.deletemeasure', function() {

    var img_id = jQuery(this).attr("data-id");

    jQuery.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        data: 'action=delete_measureimg&mimg_id=' + img_id,

        success: function(data) {

            jQuery("#mesureMentprintMe").html(data);

        }
    });

});

// jQuery('#mesureMentprintMe .favButtons .view').each(function() {

//     jQuery(this).on("click", function(e){
//         let button = e.currentTarget;
//     let element = document.querySelector('.overlayMeasure');
//     element ? element.remove() : console.log('not findout') ;

//     var product = `<div class="overlayMeasure"><div class='measurepopup'>
//     <div class='row'>
//     <div class='col-lg-6'>
//     <h3>${button.getAttribute("data-title")}</h3>
//     </div>
//     <div class='col-lg-6 sharingBox'>
//     <a href='#'>
//     <i class='fa fa-share-alt' aria-hidden='true'></i>
//             </a>

//             <a href='#'>
//                 <i class='fa fa-print' aria-hidden='true'></i>
//             </a>
//             <a href='javascript:void(0);' onclick="closePopup()">
//                 <i class='fa fa-times' aria-hidden='true'></i>
//             </a>	
//         </div>
//     </div>

//      <img src='${button.getAttribute("data-img")}' </div> </div>`;

//     jQuery('body').append(product);
//     });



// });


function openPopUp(e) {

    let title = e.currentTarget;
    console.log(e);
    let element = document.querySelector('.overlayMeasure');
    element ? element.remove() : console.log('not findout');

    var product = `<div class="overlayMeasure"><div class='measurepopup'>
    <div class='row'>
    <div class='col-lg-6'>
    <h3>${e.getAttribute("data-title")}</h3>
    </div>
    <div class='col-lg-6 sharingBox'>
    <a href='#'>
    <i class='fa fa-share-alt' aria-hidden='true'></i>
            </a>
                
            <a href='#' onclick="printDiv(measureprint)">
                <i class='fa fa-print' aria-hidden='true'></i>
            </a>
            <a href='javascript:void(0);' onclick="closePopup()">
                <i class='fa fa-times' aria-hidden='true'></i>
            </a>	
        </div>
    </div>
    
    <div id="measureprint">
     <img src='${e.getAttribute("data-img")}' </div></div> </div>`;

    jQuery('body').append(product);
}

function closePopup() {
    document.querySelector('.overlayMeasure').remove();
}

var debounce = (info, delay) => {
    let debounceTimer
    return function() {
        clearTimeout(debounceTimer)
        debounceTimer = setTimeout(() => addRemovefavAjax(info), delay)
    }
}

function addRemovefavAjax(classcontainer) {
    console.log('run run');
    if (classcontainer.status == 'add') {
        jQuery.ajax({
            type: "POST",
            url: "/wp-admin/admin-ajax.php",
            data: 'action=add_favroiute&post_id=' + classcontainer.postId + '&user_id=' + classcontainer.UserId,

            success: function(data) {
                jQuery(classcontainer.element).removeClass("add_Fav");
                jQuery(classcontainer.element).addClass("rem_fav");
                jQuery(classcontainer.element).children("i").removeClass("fa-heart-o");
                jQuery(classcontainer.element).children("i").addClass("fa-heart");

            }
        });
    } else {
        jQuery.ajax({
            type: "POST",
            url: "/wp-admin/admin-ajax.php",
            data: 'action=remove_favroiute&post_id=' + classcontainer.postId + '&user_id=' + classcontainer.UserId,

            success: function(data) {
                jQuery(classcontainer.element).removeClass("rem_fav");
                jQuery(classcontainer.element).addClass("add_Fav");
                jQuery(classcontainer.element).children("i").removeClass("fa-heart");
                jQuery(classcontainer.element).children("i").addClass("fa-heart-o");

            }
        });
    }

}

jQuery(document).on('click', '.favProdPdp', function(event) {
    event.stopPropagation();
    console.log(event)
    var current = this;
    var post_id = jQuery(this).attr("data-id");

    var classContainer = {
        postId: jQuery(this).attr("data-id"),
        UserId: jQuery(this).attr("data-user"),
        element: this
    }

    if (jQuery(current).hasClass("add_Fav")) {
        classContainer.status = "add";
        var add = debounce(classContainer, 300);
        add();
    } else {
        classContainer.status = "remove";
        var remove = debounce(classContainer, 300);
        remove();
    }
});


jQuery(document).on('click', '#rem_fav', function() {

    var img_id = jQuery(this).attr("data-id");

    jQuery.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        data: 'action=remove_favroiute&post_id=' + jQuery(this).attr("data-id") + '&user_id=' + jQuery(this).attr("data-user_id"),

        success: function(data) {
            jQuery(classcontainer.element).removeClass("rem_fav");
            jQuery(classcontainer.element).addClass("add_Fav");
            jQuery(classcontainer.element).children("i").removeClass("fa-heart");
            jQuery(classcontainer.element).children("i").addClass("fa-heart-o");

        }
    });

});