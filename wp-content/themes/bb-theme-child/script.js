/* search bar toggle js start here */


jQuery(document).ready(function(){
	
	
/* for Instock color slider */
jQuery('.color_variations_slider_default-instock > .slides').slick({
    dots: false,
    infinite: false,
    speed: 300,
    arrows: true,
    rows: 1,
    slidesPerRow: 1,
    slidesToShow: 6,
    mobileFirst: false,
    prevArrow: '<a href="javascript:void(0)" class="arrow slick-prev">Previous</a>',
    nextArrow: '<a href="javascript:void(0)" class="arrow slick-next">Next</a>',
    responsive: [{
            breakpoint: 1024,
            settings: {
                rows: 2,
                slidesPerRow: 1
            }
        },
        {
            breakpoint: 769,
            settings: {
                rows: 2,
                slidesPerRow: 1,
                slidesToShow: 3,
                slidesToScroll: 3
            }
        },
        {
            breakpoint: 480,
            settings: {
                rows: 2,
                slidesPerRow: 1,
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }

    ]
});
	
    jQuery('.custom_searchHeader .fl-icon').click(function(){
    	jQuery('.custom_searchModule').slideToggle();
    }) 

	var customSearch = jQuery('.custom_searchHeader .fl-icon , .custom_searchModule');

	jQuery(document).mouseup(function (e) {
		if (!customSearch.is(e.target) && customSearch.has(e.target).length === 0 && jQuery('.custom_searchModule').css('display') == 'block') {
			jQuery('.custom_searchModule').slideToggle();
		}
	});
})

/* search bar toggle js end here */


/*store location start*/

// header flyer choose location js start here 
jQuery(document).on('click', '.choose_location', function() {
    //alert('mystore');
    var mystore = jQuery(this).attr("data-id");
    // alert('mystore');
    var distance = jQuery(this).attr("data-distance");

    jQuery.cookie("greatfloors_store", null, { path: '/' });
    jQuery.cookie("greatfloors_distance", null, { path: '/' });

    jQuery.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        data: 'action=choose_location&store_id=' + jQuery(this).attr("data-id") + '&distance=' + jQuery(this).attr("data-distance") + '&phone=' + jQuery(this).attr("data-phone"),

        success: function(data) {

            jQuery(".header_store").html(data);
            jQuery.cookie("greatfloors_store", mystore, { expires: 1, path: '/' });
            jQuery.cookie("greatfloors_distance", distance, { expires: 1, path: '/' });
            setTimeout(addFlyerEvent, 1000);

        }
    });

});

// flyer close function 
function closeNav() {
    let storeLocation = document.getElementById("storeLocation");
    storeLocation.style.right = "-100%";
    storeLocation.classList.remove("ForOverlay");
}

// flyer event assign function
function addFlyerEvent() {
    var flyerOpener = document.getElementById('openFlyer');
    if(flyerOpener){
        flyerOpener.addEventListener("click", function(e) {
            let storeLocation = document.getElementById("storeLocation");
            storeLocation.style.right = "0";
            storeLocation.classList.add("ForOverlay");
        });
    }
   
}

jQuery(document).ready(function() {

    jQuery(function($) {
        $('#storeLocation .choose_location').on('click', closeNav);

    });

    var mystore = jQuery.cookie("greatfloors_store");
    var distance = jQuery.cookie("greatfloors_distance");

    jQuery.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        data: 'action=get_storelisting',

        success: function(data) {

            jQuery(".header_store").html(data);
            setTimeout(addFlyerEvent, 1000);
            var mystore_loc = jQuery(".contentFlyer h3").html();
            jQuery("#input_22_13").val(mystore_loc);
            jQuery("#input_24_12").val(mystore_loc);
            jQuery("#input_30_12").val(mystore_loc);
            jQuery("#input_16_9").val(mystore_loc);
            jQuery("#input_19_16").val(mystore_loc);
            jQuery("#input_34_15").val(mystore_loc);
            jQuery("#input_32_15").val(mystore_loc);
            jQuery("#input_35_15").val(mystore_loc);
            jQuery("#input_36_15").val(mystore_loc);
            jQuery("#input_37_15").val(mystore_loc);
            jQuery("#input_38_15").val(mystore_loc);
        }
    });

    jQuery.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        data: 'action=choose_location_listing',

        success: function(data) {

            jQuery("#ajaxstorelisting").html(data);
            setTimeout(addFlyerEvent, 1000);
            $('#storeLocation .choose_location').on('click', closeNav);
            var mystore_loc = jQuery(".contentFlyer h3").html();
            jQuery("#input_22_13").val(mystore_loc);
            jQuery("#input_24_12").val(mystore_loc);
            jQuery("#input_30_12").val(mystore_loc);
            jQuery("#input_16_9").val(mystore_loc);
            jQuery("#input_19_16").val(mystore_loc);
            jQuery("#input_34_15").val(mystore_loc);
            jQuery("#input_32_15").val(mystore_loc);
            jQuery("#input_35_15").val(mystore_loc);
            jQuery("#input_36_15").val(mystore_loc);
            jQuery("#input_37_15").val(mystore_loc);
            jQuery("#input_38_15").val(mystore_loc);
        }
    });

    jQuery.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        data: 'action=choose_location_listing',

        success: function(data) {

            jQuery("#ajaxstorelisting").html(data);
            setTimeout(addFlyerEvent, 1000);
            $('#storeLocation .choose_location').on('click', closeNav);
            var mystore_loc = jQuery(".contentFlyer h3").html();
            // alert(mystore_loc);

            jQuery("#input_22_13").val(mystore_loc);
            jQuery("#input_24_12").val(mystore_loc);
            jQuery("#input_30_12").val(mystore_loc);
            jQuery("#input_16_9").val(mystore_loc);
            jQuery("#input_19_16").val(mystore_loc);
            jQuery("#input_34_15").val(mystore_loc);
            jQuery("#input_32_15").val(mystore_loc);
            jQuery("#input_35_15").val(mystore_loc);
            jQuery("#input_36_15").val(mystore_loc);
            jQuery("#input_37_15").val(mystore_loc);
            jQuery("#input_38_15").val(mystore_loc);


        }
    });

    /*store location end*/

});

function addFavProduct(is_fav, sku, prod_id) {

    var queryString = "";
    queryString = 'action=add_fav_product&sku=' + sku + '&post_id=' + prod_id + '&is_fav=' + is_fav;
    jQuery.ajax({
        url: "/wp-admin/admin-ajax.php",
        data: queryString,
        type: "POST",
        dataType: "json",
        success: function(response) {

            if (is_fav == 0) {
                is_fav1 = 1;
            } else {
                is_fav1 = 0;
            }

            $(".is_fav" + sku).attr("onClick", "addFavProduct('" + is_fav1 + "','" + sku + "','" + prod_id + "')");
            $(".is_fav" + sku).removeClass("is_fav0");
            $(".is_fav" + sku).removeClass("is_fav1");
            $(".is_fav" + sku).toggleClass("is_fav" + is_fav1);
        },
        error: function() {}
    });
}



function getDataUrl(img) {
    // Create canvas
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');
    // Set width and height
    canvas.width = img.width;
    canvas.height = img.height;
    // Draw the image
    ctx.drawImage(img, 0, 0);
    return canvas.toDataURL('image/jpeg');
}
function getDataUrlTwo(img) {
    // Create canvas
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');
    // Set width and height
    canvas.width = 200;
    canvas.height = 200;
    // Draw the image
    ctx.drawImage(img, 0, 0);
    return canvas.toDataURL('image/jpeg');
}
function provideUrls(){
    if(document.querySelector('.printDOc')){
        document.querySelector('.printDOc').addEventListener('click', function(event){
            event.preventDefault()
            var img = document.querySelectorAll('.printWrap img');
          
            img.forEach(async function(ele){
                ele.setAttribute("crossOrigin", "Anonymous");
                ele.src = await getDataUrlTwo(ele);
            });
            
            document.querySelector('.hideShare').style.display = 'none';
            printDiv();
            document.querySelector('.hideShare').style.display = 'flex';
        });
    }
}
function provideUrlsTwo(){
        document.querySelector('.printDOc').addEventListener('click', function(){
            var img = document.querySelector('.printWrap img');
            img.src = getDataUrl(img);
            printDiv();
        });
}
window.onload = setTimeout(provideUrls, 1000);


function printDiv() {
    var printContents = document.querySelector(".printWrap");
    var opt = {
        margin:       0.3,
        filename:     'download.pdf',
        image:        { type: 'jpeg', quality: 0.98 },
        html2canvas:  { scale: 2 },
        jsPDF:        { unit: 'in', format: 'letter', orientation: 'portrait' }
      };
       
      // New Promise-based usage:
      html2pdf().from(printContents).set(opt).save();
}

jQuery(document).on('click', '.deletemeasure', function() {

    var img_id = jQuery(this).attr("data-id");

    jQuery.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        data: 'action=delete_measureimg&mimg_id=' + img_id,

        success: function(data) {

            jQuery("#mesureMentprintMe").html(data);

        }
    });

});


function openPopUp(e) {

    let title = e.currentTarget;
    let element = document.querySelector('.overlayMeasure');
    element ? element.remove() : console.log('not findout');

    var product = `<div class="overlayMeasure"><div class='measurepopup '>
    <div class='row'>
    <div class='col-lg-6'>
    <h3>${e.getAttribute("data-title")}</h3>
    </div>
    <div class='col-lg-6 sharingBox hideShare'>
    <a href='#' id="share-button-3" class="need-share-button-default" data-share-position="middleBottom" data-share-networks="Mailto,Twitter,Pinterest,Facebook,GooglePlus,Linkedin">
    </a>
            <a href='#' class="printDOc">
                <i class='fa fa-print' aria-hidden='true'></i>
            </a>
            <a href='javascript:void(0);' onclick="closePopup()">
                <i class='fa fa-times' aria-hidden='true'></i>
            </a>	
        </div>
    </div>
    
    <div id="measureprint " class="printWrap"> 
     <img src='${e.getAttribute("data-img")}' </div></div> </div>`;

    jQuery('body').append(product);
    provideUrlsTwo();
    //new needShareDropdown(document.getElementById('share-button-3'));

    //new needShareDropdown(document.getElementById('share-button-3'));
    removeInnerHtml();
}

function removeInnerHtml(){
    var button = document.querySelector('.need-share-button_button');
    if(button){
        button.innerHTML = '';
    }
}
removeInnerHtml();


window.onload = function checkShareBox(){
    var button = document.querySelector('#share-button-3');
    if(button){
        //new needShareDropdown(document.getElementById('share-button-3'));
        removeInnerHtml();
    }
}


function closePopup() {
    document.querySelector('.overlayMeasure').remove();
}

var debounce = (info, delay) => {
    let debounceTimer
    return function() {
        clearTimeout(debounceTimer)
        debounceTimer = setTimeout(() => addRemovefavAjax(info), delay)
    }
}

function addRemovefavAjax(classcontainer) {
    console.log('run run');
    if (classcontainer.status == 'add') {
        jQuery.ajax({
            type: "POST",
            url: "/wp-admin/admin-ajax.php",
            data: 'action=add_favroiute&post_id=' + classcontainer.postId + '&user_id=' + classcontainer.UserId,

            success: function(data) {
                jQuery(classcontainer.element).removeClass("add_Fav");
                jQuery(classcontainer.element).addClass("rem_fav");
                jQuery(classcontainer.element).children("i").removeClass("fa-heart-o");
                jQuery(classcontainer.element).children("i").addClass("fa-heart");

            }
        });
    } else {
        jQuery.ajax({
            type: "POST",
            url: "/wp-admin/admin-ajax.php",
            data: 'action=remove_favroiute&post_id=' + classcontainer.postId + '&user_id=' + classcontainer.UserId,

            success: function(data) {
                jQuery(classcontainer.element).removeClass("rem_fav");
                jQuery(classcontainer.element).addClass("add_Fav");
                jQuery(classcontainer.element).children("i").removeClass("fa-heart");
                jQuery(classcontainer.element).children("i").addClass("fa-heart-o");

            }
        });
    }

}

jQuery(document).on('click', '.favProdPdp', function(event) {
    event.stopPropagation();
    console.log(event)
    var current = this;
    var post_id = jQuery(this).attr("data-id");

    var classContainer = {
        postId: jQuery(this).attr("data-id"),
        UserId: jQuery(this).attr("data-user"),
        element: this
    }

    if (jQuery(current).hasClass("add_Fav")) {
        classContainer.status = "add";
        var add = debounce(classContainer, 300);
        add();
    } else {
        classContainer.status = "remove";
        var remove = debounce(classContainer, 300);
        remove();
    }
});


jQuery(document).on('click', '#rem_fav', function() {
    jQuery.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        data: 'action=remove_favroiute_list&post_id=' + jQuery(this).attr("data-id") + '&user_id=' + jQuery(this).attr("data-user_id"),

        success: function(data) {
            jQuery("#ajaxreplace").html(data);
            location.reload();
        }
    });
});








jQuery(".add_note_fav").each(function() {
    jQuery(this).on("click", function() {
        var proid = jQuery(this).attr("data-productid");
        jQuery('#addnote_productid').val(proid);
    });
})
jQuery(".view_note_fav").each(function() {
    jQuery(this).on("click", function() {
        var note_val = jQuery(this).attr("data-note");
        jQuery('.view_note_wrapper-overlay .uabb-modal-content-data p').text(note_val);
    });

});



jQuery('#add_note_form').submit(function(e) {
    e.preventDefault();

    var message = jQuery('textarea#message').val();
    var proid = jQuery('#addnote_productid').val();
    //alert('message');
    message = jQuery.trim(message);
    if (message) {
        jQuery.ajax({
            data: { action: 'add_note_form', note: message, addnote_productid: proid },
            type: 'post',
            url: "/wp-admin/admin-ajax.php",
            success: function(data) {
                console.log(data);
                location.reload();
            }
        });
    }
})

function ValidateTextarea(e) {
    console.log('Hiii');
}