<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
   
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","");
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","");
    wp_enqueue_script("PDF_script","https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.2/html2pdf.bundle.min.js","","");
    wp_enqueue_script("m2FlooringCalculator","https://calculator.measuresquare.com/scripts/jquery-m2FlooringCalculator.js","","");
   // wp_enqueue_script("shareBox-script", get_stylesheet_directory_uri()."/resources/sharebox/needsharebutton.js","","");
     wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","");
});




// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );

// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');

// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');

function fr_img($id=0,$size="",$url=false,$attr=""){

    //Show a theme image
    if(!is_numeric($id) && is_string($id)){
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if(file_exists(to_path($img))){
            if($url){
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if(!$id){
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if(is_object($id)){
        if(!empty($id->ID)){
            $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if(get_post_type($id)!="attachment"){
        $id=get_post_thumbnail_id($id);
    }

    if($id){
        $image_url=wp_get_attachment_image_url($id,$size);
        if(!$url){
            //If image is a SVG embed the contents so we can change the color dinamically
            if(substr($image_url,-4,4)==".svg"){
                $image_url=str_replace(get_bloginfo("url"),ABSPATH."/",$image_url);
                $data=file_get_contents($image_url);
                echo strstr($data,"<svg ");
            }else{
                return wp_get_attachment_image($id,$size,0,$attr);
            }
        }else if($url){
            return $image_url;
        }
    }
}

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );

//get ipaddress of visitor

function getUserIpAddr() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
	
	if ( strstr($ipaddress, ',') ) {
            $tmp = explode(',', $ipaddress,2);
            $ipaddress = trim($tmp[1]);
    }
    return $ipaddress;
}


// Custom function for lat and long

function get_storelisting() {
    
    global $wpdb;
    $content="";


  //  echo 'no cookie';
    
    $urllog = 'https://global-ds.cloud.netacuity.com/webservice/query';
    //12.68.65.195
    $response = wp_remote_post( $urllog, array(
        'method' => 'GET',
        'timeout' => 45,
        'redirection' => 5,
        'httpversion' => '1.0',
        'headers' => array('Content-Type' => 'application/json'),         
        'body' =>  array('u'=> 'b25d7667-74cc-4fcc-9adf-7b5f4f8f5bd0','ip'=> getUserIpAddr(),'dbs'=> 'all','trans_id'=> 'example','json'=> 'true' ),
        'blocking' => true,               
        'cookies' => array()
        )
    );
        
        $rawdata = json_decode($response['body'], true);
        $userdata = $rawdata['response'];

        $autolat = $userdata['pulseplus-latitude'];
        $autolng = $userdata['pulseplus-longitude'];
        if(isset($_COOKIE['greatfloors_store'])){

            $store_location = $_COOKIE['greatfloors_store'];
        }else{

            $store_location = '';
           
          }

        //print_r($rawdata);
        
        $sql =  "SELECT post_lat.meta_value AS lat,post_lng.meta_value AS lng,posts.ID, 
                ( 3959 * acos( cos( radians(".$autolat." ) ) * cos( radians( post_lat.meta_value ) ) * 
                cos( radians( post_lng.meta_value ) - radians( ".$autolng." ) ) + sin( radians( ".$autolat." ) ) * 
                sin( radians( post_lat.meta_value ) ) ) ) AS distance FROM wp_posts AS posts
                INNER JOIN wp_postmeta AS post_lat ON post_lat.post_id = posts.ID AND post_lat.meta_key = 'latitude'
                INNER JOIN wp_postmeta AS post_lng ON post_lng.post_id = posts.ID AND post_lng.meta_key = 'longitue'  
                WHERE posts.post_type = 'store-locations' 
                AND posts.post_status = 'publish' GROUP BY posts.ID HAVING distance < 50000000000000000000 ORDER BY distance";

        $storeposts = $wpdb->get_results($sql);
        $storeposts_array = json_decode(json_encode($storeposts), true);      

       
        if($store_location ==''){
            //write_log('empty loc');           
            $store_location = $storeposts_array['0']['ID'];
            $store_distance = round($storeposts_array['0']['distance'],1);
        }else{

           // write_log('noempty loc');
            $key = array_search($store_location, array_column($storeposts_array, 'ID'));
            $store_distance = round($storeposts_array[$key]['distance'],1);           

        }
        
        $content .= '<div class="header_store"><div class="store_wrapper">';
        $content .='<div class="locationWrapFlyer">
        <div class="icons">
            <i class="uabb-button-icon uabb-marketing-button-icon all_before ua-icon ua-icon-map-pin"></i>
        </div>
        <div class="contentFlyer">
            <p>You\'re Shopping</p>';
        $content .= '<h3 class="title-prefix">'.get_the_title($store_location).'</h3>';
		$content .= '<p class="store-phone"><a href="tel:'.get_field('phone', $store_location).'"><i class="fa fa-phone-alt"></i> &nbsp;'.get_field('phone', $store_location).'</a></p>';
        // $content .= '<a href="javascript:void(0)" target="_self" id="openFlyer" class="store-cta-link headeropenFlyer"><h5 class="title-prefix">'.get_the_title($store_location).'  - <b>'.$store_distance.' MI</b><i class="fa fa-caret-down" aria-hidden="true"></i></h5></a>';
        // $content .= '<h5 class="store-add">'.get_field('address', $store_location).' '.get_field('city',$store_location).', '.get_field('state',$store_location).' '.get_field('postal_code', $store_location).'</h5>';
        //$content .= '<p class="store-hour">'.get_field('hours', $store_location).' | '.get_field('phone', $store_location).'</p>';
        
        if(get_field('closed', $store_location)){
            // $content .= '<p class="store-hour redtext">'.get_field('hours', $store_location).'</p>';
            // $content .= '<p>Still happly serving this area in the meanwhile at our Humble location. Please call for details</p>';
        }else{
            // $content .= '<p class="store-hour">'.get_field('hours', $store_location).'</p>';
        }
        // $content .= '<p class="store-hour"><i class="fa fa-phone-alt"></i> &nbsp;'.get_field('phone', $store_location).'</p>';
       // $content .= '<a href="javascript:void(0)" target="_self" id="openFlyer" class="store-cta-link headeropenFlyer">Change Location</a>';
        $content .= '<h5 class="title-prefix" style="font-style: italic;font-size:12px;"><a href="javascript:void(0)" target="_self" id="openFlyer" class="store-cta-link headeropenFlyer">View All Locations</a></h5>';
        $content .= '</div></div>';
		
        echo $content;
        wp_die();
}

//add_shortcode('storelisting', 'get_storelisting');
add_action( 'wp_ajax_nopriv_get_storelisting', 'get_storelisting' );
add_action( 'wp_ajax_get_storelisting', 'get_storelisting' );



function choose_location_listing(){

    global $wpdb;
    
    $urllog = 'https://global-ds.cloud.netacuity.com/webservice/query';
    //12.68.65.195
    $response = wp_remote_post( $urllog, array(
        'method' => 'GET',
        'timeout' => 45,
        'redirection' => 5,
        'httpversion' => '1.0',
        'headers' => array('Content-Type' => 'application/json'),         
        'body' =>  array('u'=> 'b25d7667-74cc-4fcc-9adf-7b5f4f8f5bd0','ip'=> getUserIpAddr(),'dbs'=> 'all','trans_id'=> 'example','json'=> 'true' ),
        'blocking' => true,               
        'cookies' => array()
        )
    );

        $rawdata = json_decode($response['body'], true);
        $userdata = $rawdata['response'];
        $autolat = $userdata['pulseplus-latitude'];
        $autolng = $userdata['pulseplus-longitude'];
        
        $sql =  "SELECT post_lat.meta_value AS lat,post_lng.meta_value AS lng,posts.ID, 
                ( 3959 * acos( cos( radians(".$autolat." ) ) * cos( radians( post_lat.meta_value ) ) * 
                cos( radians( post_lng.meta_value ) - radians( ".$autolng." ) ) + sin( radians( ".$autolat." ) ) * 
                sin( radians( post_lat.meta_value ) ) ) ) AS distance FROM wp_posts AS posts
                INNER JOIN wp_postmeta AS post_lat ON post_lat.post_id = posts.ID AND post_lat.meta_key = 'latitude'
                INNER JOIN wp_postmeta AS post_lng ON post_lng.post_id = posts.ID AND post_lng.meta_key = 'longitue'  
                WHERE posts.post_type = 'store-locations' 
                AND posts.post_status = 'publish' GROUP BY posts.ID HAVING distance < 50000000000000000 ORDER BY distance LIMIT 0, 25";

        $storeposts = $wpdb->get_results($sql);

        $storeposts_array = json_decode(json_encode($storeposts), true);  

    $content = '<div id="storeLocation" class="changeLocation">';
    $content .= '<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>';
    $content .= '<div class="content">'; 
  
    foreach ( $storeposts as $post ) {
     
        $content .= '<div class="store_wrapper" id ="'.$post->ID.'">';
        $content .= '<h5 class="title-prefix">'.get_the_title($post->ID).'  - <b>'.round($post->distance,1).' MI</b></h5>';
        $content .= '<h5 class="store-add">'.get_field('address', $post->ID).' <br />'.get_field('city',$post->ID).', '.get_field('state',$post->ID).' '.get_field('postal_code', $post->ID).'</h5>';
        
        if(get_field('closed', $post->ID)){
            $content .= '<p class="store-hour redtext">'.get_field('hours', $post->ID).'</p>';
            $content .= '<p>Still happly serving this area in the meanwhile at our Humble location. Please call for details</p>';
        }else{
            $rows = get_field('store_hours',$post->ID);
            if( $rows ) {
                foreach( $rows as $row ) {
                    $day = $row['day'];
                  // write_log(date("l"));
                   // write_log($day); write_log($row);
                    if($day == date("l")){

                        if($row['is_closed'] == 1){

                        $content .= '<p class="store-hour">CLOSED TODAY</p>';   

                        }else{

                        $content .= '<p class="store-hour">OPEN UNTIL '.$row['close_time'].'</p>';

                        }

                    }

                }
            }
            
        }
        $content .= '<p class="store-hour"><i class="fa fa-phone-alt"></i> &nbsp;'.get_field('phone', $post->ID).'</p>';
        $content .= '<a href="javascript:void(0)" data-id="'.$post->ID.'" data-distance="'.round($post->distance,1).'" data-phone="'.get_field('phone', $post->ID).'" target="_self" class="store-cta-link choose_location">Choose This Location</a>';
        if(get_field('store_page_link',$post->ID)){
            $content .= '<br><a href="'.get_field('store_page_link',$post->ID).'" target="_self" class="store-cta-link view_location"> View Location</a>';
        }
        $content .= '</div>'; 
    }
    
    $content .= '</div>';
    $content .= '</div>';

    echo $content;
    wp_die();
}

//add_shortcode('chooselisting', 'choose_location_listing');
add_action( 'wp_ajax_nopriv_choose_location_listing', 'choose_location_listing' );
add_action( 'wp_ajax_choose_location_listing', 'choose_location_listing' );

//choose this location FUnctionality

add_action( 'wp_ajax_nopriv_choose_location', 'choose_location' );
add_action( 'wp_ajax_choose_location', 'choose_location' );

function choose_location() {
   
        $content = '<div class="store_wrapper">';
        $content .='<div class="locationWrapFlyer">
        <div class="icons">
            <i class="uabb-button-icon uabb-marketing-button-icon all_before ua-icon ua-icon-map-pin"></i>
        </div>
        <div class="contentFlyer">
            <p>You\'re Shopping</p>
            
           ';
        $content .= '<h3 class="title-prefix">'. get_the_title($_POST['store_id']) .'</h3>';
		$content .= '<p class="store-phone"><a href="tel:'.get_field('phone', $_POST['store_id']).'"><i class="fa fa-phone-alt"></i> &nbsp;'.get_field('phone', $_POST['store_id']).'</a></p>';
        // $content .= '<a href="javascript:void(0)" target="_self" id="openFlyer" class="store-cta-link headeropenFlyer"><h5 class="title-prefix">'.get_the_title($_POST['store_id']).'  - <b>'.$_POST['distance'].' MI</b><i class="fa fa-caret-down" aria-hidden="true"></i></h5></a>';
        // $content .= '<h5 class="store-add">'.get_field('address', $_POST['store_id']).' '.get_field('city',$_POST['store_id']).', '.get_field('state',$_POST['store_id']).' '.get_field('postal_code', $_POST['store_id']).'</h5>';
        

        if(get_field('closed', $_POST['store_id'])){
            // $content .= '<p class="store-hour redtext">'.get_field('hours',$_POST['store_id']).'</p>';
            // $content .= '<p>Still happly serving this area in the meanwhile at our Humble location. Please call for details</p>';
            // $content .= '<p class="store-hour"> '.get_field('phone', $_POST['store_id']).'</p>';
        }else{
           // $content .= '<p class="store-hour">'.get_field('hours',$_POST['store_id']).' | '.get_field('phone', $_POST['store_id']).'</p>';
        }

       // $content .= '<a href="javascript:void(0)" target="_self" id="openFlyer" class="store-cta-link">Change Location</a>';
       $content .= '<h5 class="title-prefix" style="font-style: italic;font-size:12px;"><a href="javascript:void(0)" target="_self" id="openFlyer" class="store-cta-link headeropenFlyer">View All Locations</a></h5>';
        $content .= '</div>';

        echo $content;

    wp_die();
}

add_filter( 'facetwp_template_html', function( $output, $class ) {
    //$GLOBALS['wp_query'] = $class->query;
    $prod_list = $class->query;  
    ob_start();       
   // write_log($class->query_args['check_instock']);
    if(isset($class->query_args['check_instock']) && $class->query_args['check_instock'] == 1){

        $dir = get_stylesheet_directory().'/product-instock-loop-great-floors.php';

    }else{

        $dir = get_stylesheet_directory().'/product-loop-great-floors.php';
    }
   
    require_once $dir;    
    return ob_get_clean();
}, 10, 2 );





add_action( 'wp_ajax_nopriv_add_fav_product', 'add_fav_product' );
add_action( 'wp_ajax_add_fav_product', 'add_fav_product' );

function add_fav_product() {
    $is_fav = $_POST['is_fav'];
    $post_id = $_POST['post_id'];

    $result = update_post_meta( $post_id, 'is_fav', $is_fav);
if($result == false){
    $result1 = add_post_meta( $post_id, 'is_fav', $is_fav);
}
}

add_action( 'wp_ajax_nopriv_base64_to_jpeg_convert', 'base64_to_jpeg_convert' );
add_action( 'wp_ajax_base64_to_jpeg_convert', 'base64_to_jpeg_convert' );

function base64_to_jpeg_convert() {    
    global $wpdb;

    $upload_dir = wp_get_upload_dir();
    $output_file= $upload_dir['basedir']. '/measure/'.uniqid().'.png';

    $img = $_POST['imagedata']; 
    $img = str_replace('data:image/png;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    file_put_contents($output_file, $data);
    
    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
      //  write_log( 'Personal Message For '. $current_user->ID ) ;
     
        $data=array(
            'userid' => $current_user->ID, 
            'image_name' => $_POST['imagename'],
            'image_path' => $output_file 
        );    
    
         $wpdb->insert( 'wp_measure_images', $data);

         $measureimages = $wpdb->get_results("SELECT * FROM wp_measure_images WHERE userid = $current_user->ID ");

       // write_log($measureimages);

        
        $content = '<div id="mesureMentprintMe" class="product-plp-grid product-grid swatch "><div class="row product-row" id="aniimated-thumbnials">';

        $content .= '<h3 style="text-align: center; font-size: 33px;"><span>SAVED MEASUREMENTS</span></h3>';

        foreach($measureimages as $img){

           

            $img_path = str_replace('/var/www/html', '', $img->image_path);

            $image_url = home_url().''.$img_path;

            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap mesureMentWrap">';

            $content .= '<a href="javascript:void(0);" title="#" class="wplightbox"><img class="measure_img" src="'.home_url().''.$img_path.'" /></a>';

            $content .= '<div class="favButtons button-wrapper">
                <a class="button fl-button view" href="javascript:void(0);" title="#"  data-img="'.home_url().''.$img_path.'" onclick="openPopUp(this)" data-title="'.$img->image_name.'">VIEW</a>';
                $content .= '<a class="button fl-button deletemeasure" href="javascript:void(0)" data-id="'.$img->id.'"><i class="fa fa-trash" aria-hidden="true"></i></a>'; 
                $content .= '</div> </div>';
                $content .= '<h3><span>'.$img->image_name.'</span></h3>';

                $content .= "</div></div>";

        }

        $content .= "</div></div>";

        echo $content;


        wp_die();
    }

}

function measurement_tool_images($arg){    
    global $wpdb;

    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
       // write_log( 'Personal Message For '. $current_user->ID ) ;

        $measureimages = $wpdb->get_results("SELECT * FROM wp_measure_images WHERE userid = $current_user->ID ");

       // write_log($measureimages);

        
        $content = '<div id="mesureMentprintMe" class="product-plp-grid product-grid swatch "><div class="row product-row" id="aniimated-thumbnials">';
        $content .= '<h3 style="text-align: center; font-size: 33px;"><span>SAVED MEASUREMENTS</span></h3>';

        foreach($measureimages as $img){

           

            $img_path = str_replace('/var/www/html', '', $img->image_path);

            $image_url = home_url().''.$img_path;

            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap mesureMentWrap">';

            $content .= '<a href="javascript:void(0);" title="#" class="wplightbox"><img class="measure_img" src="'.home_url().''.$img_path.'" /></a>';

            $content .= '<div class="favButtons button-wrapper">
                <a class="button fl-button view" href="javascript:void(0);" title="#"  data-img="'.home_url().''.$img_path.'" onclick="openPopUp(this)" data-title="'.$img->image_name.'">VIEW</a>';
                $content .= '<a class="button fl-button deletemeasure" href="#" data-id="'.$img->id.'"><i class="fa fa-trash" aria-hidden="true"></i></a>'; 
                $content .= '</div> </div>';
                $content .= '<h3><span>'.$img->image_name.'</span></h3>';

                $content .= "</div></div>";

        }

        $content .= "</div></div>";
        

        return $content;
    }
   
  
}    
add_shortcode('measurementtool_images', 'measurement_tool_images');


add_action( 'wp_ajax_nopriv_delete_measureimg', 'delete_measurement_images' );
add_action( 'wp_ajax_delete_measureimg', 'delete_measurement_images' );

function delete_measurement_images() {  
    global $wpdb;

    $mid = $_POST['mimg_id'];

    if ( is_user_logged_in() ) {
       // $current_user = wp_get_current_user();
        write_log( 'Personal Message For '. $current_user->ID ) ;

        $wpdb->get_results('DELETE FROM wp_measure_images WHERE userid = '.$current_user->ID.' and id = '.$mid.'');
        

        $measureimages = $wpdb->get_results("SELECT * FROM wp_measure_images WHERE userid = $current_user->ID ");

       // write_log($measureimages);

        
        $content = '<div id="mesureMentprintMe" class="product-plp-grid product-grid swatch "><div class="row product-row" id="aniimated-thumbnials">';

        $content .= '<h3 style="text-align: center; font-size: 33px;"><span>SAVED MEASUREMENTS</span></h3>';

        foreach($measureimages as $img){

           

            $img_path = str_replace('/var/www/html', '', $img->image_path);

            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap mesureMentWrap">';

            $content .= '<a href="'.home_url().''.$img_path.'" title="#" class="wplightbox"><img class="measure_img" src="'.home_url().''.$img_path.'" /></a>';

            $content .= '<div class="favButtons button-wrapper">
                <a class="button fl-button" href="javascript:void(0)" title="#" onclick="openPopUp(this)"   data-img="'.home_url().''.$img_path.'" data-title="'.$img->image_name.'" onclik="openPopUp(e)">VIEW</a>';
                $content .= '<a class="button fl-button deletemeasure" href="#" data-id="'.$img->id.'"><i class="fa fa-trash" aria-hidden="true"></i></a>'; 
                $content .= '</div> </div>';
                $content .= '<h3><span>'.$img->image_name.'</span></h3>';

                $content .= "</div></div>";

        }

        $content .= "</div></div>";

        echo $content;
    }

  wp_die();

}

add_action( 'wp_ajax_nopriv_add_favroiute', 'add_favroiute' );
add_action( 'wp_ajax_add_favroiute', 'add_favroiute' );

function add_favroiute() {

    global $wpdb;

    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
       // write_log( 'Personal Message For '. $current_user->ID ) ;
     
        $data=array(
            'user_id' => $_POST['user_id'], 
            'product_id' => $_POST['post_id']           
        );    
    
         $wpdb->insert( 'wp_favorite_posts', $data);        

         return true;

         wp_die();

    }else{

        return false;

        wp_die();
    }
}

add_action( 'wp_ajax_nopriv_remove_favroiute', 'remove_favroiute' );
add_action( 'wp_ajax_remove_favroiute', 'remove_favroiute' );

function remove_favroiute() {    

    global $wpdb;

    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
       // write_log( 'Personal Message For '. $current_user->ID ) ;
     
        $data=array(
            'user_id' => $_POST['user_id'], 
            'product_id' => $_POST['post_id']           
        );    
    
        $wpdb->get_results('DELETE FROM wp_favorite_posts WHERE user_id = '.$_POST['user_id'].' and product_id = '.$_POST['post_id'].'');

         return true;

         wp_die();

    }else{

        return false;

        wp_die();
    }

}


function greatcustom_favorite_posts_function(){

    global $wpdb;

    $content = "";

    $fav_sql = 'SELECT product_id FROM wp_favorite_posts WHERE user_id = '.get_current_user_id().'';
            
    $check_fav = $wpdb->get_results($fav_sql);    

   // write_log($check_fav);

    $fav_products = array();

    foreach($check_fav as $fav){

        $fav_products[] = $fav->product_id;
    }

   // write_log('dsfsd'.$fav_products);
    
    $favorite_post_ids = array_reverse($fav_products);
    $page = intval(get_query_var('paged'));

    $post_array = array('carpeting','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','tile_catalog', 'instock_laminate');

        $brandmapping = array(
            "Carpet"=>"carpeting",
            "Hardwood"=>"hardwood_catalog",
            "Laminate"=>"laminate_catalog",
            "Instock Laminate"=>"instock_laminate",
            "Luxury Vinyl Tile"=>"luxury_vinyl_tile",
            "Ceramic Tile"=>"tile_catalog",
            "Waterproof"=>"solid_wpc_waterproof"
        );
    

   

    $content .= '<div id="ajaxreplace"><div class="favProWarpper printWrap">
                    <div class="shareAndPrint hideShare">
                        <a href="#" id="share-button-3" class="need-share-button-default" data-share-position="middleBottom" data-share-networks="Mailto,Twitter,Pinterest,Facebook,GooglePlus,Linkedin"></a>
                        <a href="#" class="printDOc" ><i class="fa fa-print" aria-hidden="true"></i></a>
                    </div>
                <div class="row statiContent">
                    <div class="col-lg-12">
                        <h2>MY FAVORITE PRODUCTS </h2>
                        <p>Our product catalog is home to thousands of products. 
                        Collect your favorites and save them here to help you select the right product for your home.</p>
                    </div>
                </div>';
    
    if(!empty($check_fav)) {

        
        foreach($post_array as $posttype){

            $qry = array('post__in' => $favorite_post_ids, 'posts_per_page'=> $post_per_page, 'orderby' => 'post__in', 'paged' => $page);
            // custom post type support can easily be added with a line of code like below.
           //  $qry['post_type'] = array('carpeting','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','tile_catalog');
           $qry['post_type'] = array($posttype);
           
           query_posts($qry);
    
           $post_type_name = array_search($posttype,$brandmapping);
            
           $content .= '<div id="printMe" class="product-plp-grid product-grid swatch "><div class="row product-row">';
            if ( have_posts() ) :
                $content .= '<h3>'.$post_type_name.'</h3>';
            endif;
            while ( have_posts() ) : the_post();

            $note_sql = 'SELECT note FROM wp_favorite_posts WHERE user_id = '.get_current_user_id().' and product_id='.get_the_ID().'';
            
            $check_note = $wpdb->get_results($note_sql, ARRAY_A);  
         //   write_log('check_note');
         //   write_log($check_note[0]['note']);
               
            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap">';
    
                $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
    
                $sku = get_post_meta(get_the_ID(),'sku',true);
                $manufacturer = get_post_meta(get_the_ID(),'manufacturer',true);
    
                $content .= '<a href="'.get_permalink().'" title="'.get_the_title() .'"><img crossOrigin="Anonymous" class="list-pro-image" src="'. $image.'" alt="'.get_the_title().'" /></a>';
    
                $content .= '<div class="favButtons button-wrapper">
                    <a class="button fl-button" href="'.get_permalink().'" title="'.get_the_title() .'">VIEW PRODUCT</a>';
                   // $content .=   roomvo_script_integration($manufacturer,$sku,get_the_ID());
                   if($check_note[0]['note'] == '' || $check_note[0]['note'] == null){
                    $content .= '<a class="button fl-button add_note_fav" id="add_note_fun" href="javascript:void(0)" data-productid="'.get_the_ID().'">ADD A NOTE</a>'; 
                   }
                    $content .= '<div class="buttonHolder"><a id="rem_fav" data-id="'.get_the_ID().'" class="remove-parent" data-user_id="'.get_current_user_id().'" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i></a>';

                    if($check_note[0]['note'] != '' || $check_note[0]['note'] != null){
                        $content .= '<a class="view_note_fav" data-note="'.$check_note[0]['note'].'" id="view_note_fun" href="javascript:void(0)" data-productid="'.get_the_ID().'"><i class="fa fa-sticky-note" aria-hidden="true"></i></a>'; 
                        $content .= '<div id="'.get_the_ID().'" style="display:none">'.$check_note[0]['note'].'</div>';
                       }
                    $content .= '</div></div> </div>';
                    $content .= ' <div class="fl-post-grid-text product-grid btn-grey">';
                    $content .= '<h4><span>'.get_the_title().'</span></h4>';
                    $content .= '</div>';
    
                //wpfp_remove_favorite_link(get_the_ID());
                $content .= "</div></div>";
            endwhile;
            $content .= "</div></div>";
        
    
        }
        echo '</div></div>';

    return $content;

 }


}
add_shortcode('greatcustom_favorite_posts', 'greatcustom_favorite_posts_function');


add_action( 'wp_ajax_nopriv_remove_favroiute_list', 'remove_greatcustom_favorite_posts_function' );
add_action( 'wp_ajax_remove_favroiute_list', 'remove_greatcustom_favorite_posts_function' );
function remove_greatcustom_favorite_posts_function(){

    global $wpdb;

    $content = "";

    $wpdb->get_results('DELETE FROM wp_favorite_posts WHERE product_id = '.$_POST['post_id'].' and user_id = '.get_current_user_id().'');    

    $fav_sql = 'SELECT product_id FROM wp_favorite_posts WHERE user_id = '.get_current_user_id().'';
            
    $check_fav = $wpdb->get_results($fav_sql);    

   // write_log($check_fav);

    $fav_products = array();

    foreach($check_fav as $fav){

        $fav_products[] = $fav->product_id;
    }

   // write_log('dsfsd'.$fav_products);
    
    $favorite_post_ids = array_reverse($fav_products);
    $page = intval(get_query_var('paged'));

    $post_array = array('carpeting','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','tile_catalog');

        $brandmapping = array(
            "Carpet"=>"carpeting",
            "Hardwood"=>"hardwood_catalog",
            "Laminate"=>"laminate_catalog",
            "Luxury Vinyl Tile"=>"luxury_vinyl_tile",
            "Ceramic Tile"=>"tile_catalog",
            "Waterproof"=>"solid_wpc_waterproof"
        );
    

   

    $content .= '<div class="favProWarpper printWrap">
                    <div class="shareAndPrint hideShare">
                        <a href="#" id="share-button-3" class="need-share-button-default" data-share-position="middleBottom" data-share-networks="Mailto,Twitter,Pinterest,Facebook,GooglePlus,Linkedin"></a>
                        <a href="#" class="printDOc" ><i class="fa fa-print" aria-hidden="true"></i></a>
                    </div>
                <div class="row statiContent">
                    <div class="col-lg-12">
                        <h2>MY FAVORITE PRODUCTS </h2>
                        <p>Our product catalog is home to thousands of products. 
                        Collect your favorites and save them here to help you select the right product for your home.</p>
                    </div>
                </div>';
    
    if(!empty($check_fav)) {

        
        foreach($post_array as $posttype){

            $qry = array('post__in' => $favorite_post_ids, 'posts_per_page'=> $post_per_page, 'orderby' => 'post__in', 'paged' => $page);
            // custom post type support can easily be added with a line of code like below.
           //  $qry['post_type'] = array('carpeting','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','tile_catalog');
           $qry['post_type'] = array($posttype);
           
           query_posts($qry);
    
           $post_type_name = array_search($posttype,$brandmapping);
            
           $content .= '<div id="printMe" class="product-plp-grid product-grid swatch "><div class="row product-row">';
            if ( have_posts() ) :
                $content .= '<h3>'.$post_type_name.'</h3>';
            endif;
            while ( have_posts() ) : the_post();

            $note_sql = 'SELECT note FROM wp_favorite_posts WHERE user_id = '.get_current_user_id().' and product_id='.get_the_ID().'';
            
            $check_note = $wpdb->get_results($note_sql, ARRAY_A);  
            //write_log('check_note');
          //  write_log($check_note[0]['note']);
               
            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap">';
    
                $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
    
                $sku = get_post_meta(get_the_ID(),'sku',true);
                $manufacturer = get_post_meta(get_the_ID(),'manufacturer',true);
    
                $content .= '<a href="'.get_permalink().'" title="'.get_the_title() .'"><img crossOrigin="Anonymous" class="list-pro-image" src="'. $image.'" alt="'.get_the_title().'" /></a>';
    
                $content .= '<div class="favButtons button-wrapper">
                    <a class="button fl-button" href="'.get_permalink().'" title="'.get_the_title() .'">VIEW PRODUCT</a>';
                   // $content .=   roomvo_script_integration($manufacturer,$sku,get_the_ID());
                   if($check_note[0]['note'] == '' || $check_note[0]['note'] == null){
                    $content .= '<a class="button fl-button add_note_fav" id="add_note_fun" href="javascript:void(0)" data-productid="'.get_the_ID().'">ADD A NOTE</a>'; 
                   }
                    $content .= '<div class="buttonHolder"><a id="rem_fav" data-id="'.get_the_ID().'" class="remove-parent" data-user_id="'.get_current_user_id().'" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i></a>';

                    if($check_note[0]['note'] != '' || $check_note[0]['note'] != null){
                        $content .= '<a class="view_note_fav" data-note="'.$check_note[0]['note'].'" id="view_note_fun" href="javascript:void(0)" data-productid="'.get_the_ID().'"><i class="fa fa-sticky-note" aria-hidden="true"></i></a>'; 
                        $content .= '<div id="'.get_the_ID().'" style="display:none">'.$check_note[0]['note'].'</div>';
                       }
                    $content .= '</div></div> </div>';
                    $content .= ' <div class="fl-post-grid-text product-grid btn-grey">';
                    $content .= '<h4><span>'.get_the_title().'</span></h4>';
                    $content .= '</div>';
    
                //wpfp_remove_favorite_link(get_the_ID());
                $content .= "</div></div>";
            endwhile;
            $content .= "</div></div>";
        
    
        }

        $content .='</div></div>';
        

    echo $content;

    wp_die();

 }


}


add_filter( 'gform_pre_render_32', 'populate_product_color_form' );
add_filter( 'gform_pre_validation_32', 'populate_product_color_form' );
add_filter( 'gform_pre_submission_filter_32', 'populate_product_color_form' );
add_filter( 'gform_admin_pre_render_32', 'populate_product_color_form' );
function populate_product_color_form( $form ) {
     foreach ( $form['fields'] as &$field ) {

          // Only populate field ID 12
          if( $field['id'] == 2 ) {

               $flooringtype = get_post_type(get_the_ID()); 
               $collection =  get_post_meta(get_the_ID(),'collection',true); 
			   $satur = array('Masland','Dixie Home');  
											
                if($collection != NULL ){
                    
                    if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') {

                        $familycolor = get_post_meta(get_the_ID(),'style',true); 
                        $key = 'style';

                    }else{

                        $familycolor = $collection;   
                        $key = 'collection';
                        
                    }	
                }else{	
                    
                    if (in_array(get_post_meta(get_the_ID(),'brand',true), $satur)){
                        $familycolor = get_post_meta(get_the_ID(),'design',true); 
                        $key = 'design';
                        }

                }	

                    $args = array(
                        'post_type'      => $flooringtype,
                        'posts_per_page' => -1,
                        'post_status'    => 'publish',
                        'meta_query'     => array(
                            array(
                                'key'     => $key,
                                'value'   => $familycolor,
                                'compare' => '='
                            ),
                            array(
                                'key' => 'swatch_image_link',
                                'value' => '',
                                'compare' => '!='
                                )
                        )
                    );										
            
                    $the_query = new WP_Query( $args );
                                            
                                            
               $choices = array(); // Set up blank array

              
                    // loop over each select item at add value/option to $choices array

                while ($the_query->have_posts()) {
                    $the_query->the_post();

                    $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');

                    $brand = get_post_meta(get_the_ID(),'brand',true);
                    $collection = get_post_meta(get_the_ID(),'collection',true);
                    $color = get_post_meta(get_the_ID(),'color',true);

                    $text = $brand.' '.$collection.' '.$color;
                    
                         $choices[] = array( 'text' => $text, 'price' =>  $image, 'value' => get_the_title(get_the_ID()) );

                }
                wp_reset_postdata();

               // Set placeholder text for dropdown
               $field->placeholder = '-- Choose color --';

               // Set choices from array of ACF values
               $field->choices = $choices;

          }
     }
return $form;
}


add_filter( 'gform_pre_render_34', 'populate_product_color' );
add_filter( 'gform_pre_validation_34', 'populate_product_color' );
add_filter( 'gform_pre_submission_filter_34', 'populate_product_color' );
add_filter( 'gform_admin_pre_render_34', 'populate_product_color' );

add_filter( 'gform_pre_render_35', 'populate_product_color' );
add_filter( 'gform_pre_validation_35', 'populate_product_color' );
add_filter( 'gform_pre_submission_filter_35', 'populate_product_color' );
add_filter( 'gform_admin_pre_render_35', 'populate_product_color' );

add_filter( 'gform_pre_render_38', 'populate_product_color' );
add_filter( 'gform_pre_validation_38', 'populate_product_color' );
add_filter( 'gform_pre_submission_filter_38', 'populate_product_color' );
add_filter( 'gform_admin_pre_render_38', 'populate_product_color' );


add_filter( 'gform_pre_render_36', 'populate_product_color' );
add_filter( 'gform_pre_validation_36', 'populate_product_color' );
add_filter( 'gform_pre_submission_filter_36', 'populate_product_color' );
add_filter( 'gform_admin_pre_render_36', 'populate_product_color' );

add_filter( 'gform_pre_render_37', 'populate_product_color' );
add_filter( 'gform_pre_validation_37', 'populate_product_color' );
add_filter( 'gform_pre_submission_filter_37', 'populate_product_color' );
add_filter( 'gform_admin_pre_render_37', 'populate_product_color' );

function populate_product_color( $form ) {
     foreach ( $form['fields'] as &$field ) {

          // Only populate field ID 12
          if( $field['id'] == 2 ) {

              

               $flooringtype = get_post_type(get_the_ID()); 
               $collection =  get_post_meta(get_the_ID(),'collection',true); 
			   $satur = array('Masland','Dixie Home');  
											
                if($collection != NULL ){
                    
                    if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') {

                        $familycolor = get_post_meta(get_the_ID(),'style',true); 
                        $key = 'style';

                    }else{

                        $familycolor = $collection;   
                        $key = 'collection';
                        
                    }	
                }else{	
                    
                    if (in_array(get_post_meta(get_the_ID(),'brand',true), $satur)){
                        $familycolor = get_post_meta(get_the_ID(),'design',true); 
                        $key = 'design';
                        }

                }	

                    $args = array(
                        'post_type'      => $flooringtype,
                        'posts_per_page' => -1,
                        'post_status'    => 'publish',
                        'meta_query'     => array(
                            array(
                                'key'     => $key,
                                'value'   => $familycolor,
                                'compare' => '='
                            ),
                            array(
                                'key' => 'swatch_image_link',
                                'value' => '',
                                'compare' => '!='
                                )
                        )
                    );										
            
                    $the_query = new WP_Query( $args );
                                            
                                            
               $choices = array(); // Set up blank array

              
                    // loop over each select item at add value/option to $choices array

                while ($the_query->have_posts()) {
                    $the_query->the_post();

                    $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');

                    $brand = get_post_meta(get_the_ID(),'brand',true);
                    $collection = get_post_meta(get_the_ID(),'collection',true);
                    $color = get_post_meta(get_the_ID(),'color',true);

                    $text = $brand.' '.$collection.' '.$color;
                    
                         $choices[] = array( 'text' => $text, 'price' =>  $image, 'value' => get_the_title(get_the_ID()) );

                }
                wp_reset_postdata();

               // Set placeholder text for dropdown
               $field->placeholder = '-- Choose color --';

               // Set choices from array of ACF values
               $field->choices = $choices;

          }
     }
return $form;
}



add_action( 'wp_ajax_nopriv_get_colorlisting', 'get_colorlisting_function' );
add_action( 'wp_ajax_get_colorlisting', 'get_colorlisting_function' );
function get_colorlisting_function(){

global $wpdb;
               $flooringtype = get_post_type($_POST['product_id']); 
               $collection =  get_post_meta($_POST['product_id'],'collection',true); 
			   $satur = array('Masland','Dixie Home');  
											
                if($collection != NULL ){
                    
                    if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') {

                        $familycolor = get_post_meta($_POST['product_id'],'style',true); 
                        $key = 'style';

                    }else{

                        $familycolor = $collection;   
                        $key = 'collection';
                        
                    }	
                }else{	
                    
                    if (in_array(get_post_meta($_POST['product_id'],'brand',true), $satur)){
                        $familycolor = get_post_meta($_POST['product_id'],'design',true); 
                        $key = 'design';
                        }

                }	

                    $args = array(
                        'post_type'      => $flooringtype,
                        'posts_per_page' => -1,
                        'post_status'    => 'publish',
                        'meta_query'     => array(
                            array(
                                'key'     => $key,
                                'value'   => $familycolor,
                                'compare' => '='
                            ),
                            array(
                                'key' => 'swatch_image_link',
                                'value' => '',
                                'compare' => '!='
                                )
                        )
                    );										
            
                    $the_query = new WP_Query( $args );
                                            
                                            
               $choices = array(); // Set up blank array

              
                    // loop over each select item at add value/option to $choices array

                while ($the_query->have_posts()) {
                    $the_query->the_post();

                    $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
                    
                         $choices[] = array( get_post_meta(get_the_ID(),'color',true) => $image);

                }
                wp_reset_postdata();

              //  write_log($choices);

                echo  json_encode($choices);

                wp_die();

}

add_filter( 'gform_pre_render_32', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_32', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_32', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_32', 'populate_product_location_form' );
function populate_product_location_form( $form ) {

    foreach ( $form['fields'] as &$field ) {

        // Only populate field ID 12
        if( $field['id'] == 15 ) {           	

            $args = array(
                'post_type'      => 'store-locations',
                'posts_per_page' => -1,
                'post_status'    => 'publish'
            );										
          
             $locations =  get_posts( $args );

             $choices = array(); 

             foreach($locations as $location) {
                

                  $title = get_the_title($location->ID);
                  
                       $choices[] = array( 'text' => $title, 'value' => $title );

              }
              wp_reset_postdata();

             // write_log($choices);

             // Set placeholder text for dropdown
             $field->placeholder = '-- Choose Location --';

             // Set choices from array of ACF values
             $field->choices = $choices;

        }
   }
return $form;

}


add_filter( 'gform_pre_render_34', 'populate_product_orderlocation_form' );
add_filter( 'gform_pre_validation_34', 'populate_product_orderlocation_form' );
add_filter( 'gform_pre_submission_filter_34', 'populate_product_orderlocation_form' );
add_filter( 'gform_admin_pre_render_34', 'populate_product_orderlocation_form' );
function populate_product_orderlocation_form( $form ) {

    foreach ( $form['fields'] as &$field ) {

        // Only populate field ID 12
        if( $field['id'] == 15 ) {           	

            $args = array(
                'post_type'      => 'store-locations',
                'posts_per_page' => -1,
                'post_status'    => 'publish'
            );										
          
             $locations =  get_posts( $args );

             $choices = array(); 

             foreach($locations as $location) {
                

                  $title = get_the_title($location->ID);
                  
                       $choices[] = array( 'text' => $title, 'value' => $title );

              }
              wp_reset_postdata();

              //write_log($choices);

             // Set placeholder text for dropdown
             $field->placeholder = '-- Choose Location --';

             // Set choices from array of ACF values
             $field->choices = $choices;

        }
   }
return $form;

}


function replace_core_jquery_version() {
    wp_deregister_script( 'jquery' );
    // Change the URL if you want to load a local copy of jQuery from your own server.
    wp_register_script( 'jquery', "https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js", array(), '3.2.1' );
}
add_action( 'wp_enqueue_scripts', 'replace_core_jquery_version' );

add_action('wp_ajax_add_note_form', 'add_note_form');
add_action('wp_ajax_nopriv_add_note_form', 'add_note_form');

function add_note_form()
{
    global $wpdb;

    $fav_sql = 'UPDATE wp_favorite_posts SET note = "'.$_POST['note'].'" WHERE user_id = '.get_current_user_id().' and product_id = '.$_POST['addnote_productid'].'';
            
    $check_fav = $wpdb->get_results($fav_sql); 

    echo $fav_sql;
}


add_filter( 'facetwp_facet_orderby', function( $orderby, $facet ) {
    //write_log($facet);
    global $wp_query;
    $post = $wp_query->post;
  //  Write_log($post);

    if ( 'brand' == $facet['name'] && $post->ID == 1166) {        
       
        $slug = get_post_field( 'post_name', $post->post_parent);
        $product_cat = array('carpet'=> 'FIELD(f.facet_display_value, "Mohawk", "Shaw Floors", "Masland", "Anderson Tuftex", "Karastan", "Phenix", "Dixie Home", "Fabrica", "Stanton", "Dream Weaver", "Philadelphia Commercial")');
        $orderby = $product_cat[$slug];
        
    }

    if ( 'brand' == $facet['name'] && $post->ID == 1018192) {        
       
        $slug = get_post_field( 'post_name', $post->post_parent);
        $product_cat = array('vinyl'=> 'FIELD(f.facet_display_value, "Coretec", "Mannington", "Great Floors Exclusive", "Karastan Luxecraft", "Pergo Extreme")');
        $orderby = $product_cat[$slug];
        
    }

    return $orderby;
   
   
}, 10, 2 );

// function that runs when shortcode is called
function wpb_location_shortcode() { 
 
    if($_GET['location']){

        return $_GET['location'];
    }
   
    } 
    // register shortcode
    add_shortcode('showroom', 'wpb_location_shortcode');

//     add_filter( 'facetwp_facet_render_args', function( $args ) {
//     if ( 'brand' == $args['facet']['name'] ) {
//         $translations = [
//             'Shaw Floors' => __( 'Shaw Floorte', 'fwp' )
//         ];

//         if ( ! empty( $args['values'] ) ) {
//             foreach ( $args['values'] as $key => $val ) {
//                 $display_value = $val['facet_display_value'];
//                 if ( isset( $translations[ $display_value ] ) ) {
//                     $args['values'][ $key ]['facet_display_value'] = $translations[ $display_value ];
//                 }
//             }
//         }
//     }
//     return $args;
// });

// wp_clear_scheduled_hook( 'action_scheduler_run_queue' );

// function asdds_disable_default_runner() {
//    if ( class_exists( 'ActionScheduler' ) ) {
//        remove_action( 'action_scheduler_run_queue', array( ActionScheduler::runner(), 'run' ) );
//    }
// }
// // ActionScheduler_QueueRunner::init() is attached to 'init' with priority 1, so we need to run after that
// add_action( 'init', 'asdds_disable_default_runner', 10 );


//add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
    write_log($sql_delete);	
}

add_filter( 'auto_update_plugin', '__return_false' );


//Accessibility employee shortcode
function accessibility_shortcodes($arg) {  

    $website_json =  json_decode(get_option('website_json'));
     

     foreach($website_json->contacts as $contact){

        if($contact->accessibility == '1'){ 

            if (in_array("phone", $arg)) {

                $content = '<a href="tel:'.$contact->phone.'">'.$contact->phone.'</a>';                
               
            }
            if (in_array("email", $arg)) {

                $content = '<a href="mailto:'.$contact->email.'">'.$contact->email.'</a>';              
               
            }
        }
    }

    return  $content;
    
}
add_shortcode('accessibility', 'accessibility_shortcodes');

if (! wp_next_scheduled ( 'specialsync_tile_friday_event')) {

    $interval =  getIntervalTime('friday');    
    wp_schedule_event( time(), 'each_friday', 'specialsync_tile_friday_event');
}

add_action( 'specialsync_tile_friday_event', 'special_this_friday_tile', 10, 2 );

function special_this_friday_tile() {

    $product_json =  json_decode(get_option('product_json'));     
    $tile_array = getArrayFiltered('productType','tile',$product_json);
    global $wpdb;
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/sfn-data';  

    $table_posts = $wpdb->prefix.'posts';
	$table_meta = $wpdb->prefix.'postmeta';	

    // $sql_delete = "DELETE FROM $table_meta WHERE meta_key = 'endpoint' " ;	
    
    
	// $delete_endpoint = $wpdb->get_results($sql_delete);
    
    
   foreach ($tile_array as $tile){

   if( $tile->manufacturer == "Emser"){

    $permfile = $upload_dir.'/tile_catalog_'.$tile->manufacturer.'.json';
    $res = SOURCEURL.get_option('SITE_CODE').'/www/tile/'.$tile->manufacturer.'.json?'.SFN_STATUS_PARAMETER;
   
    $tmpfile = download_url( $res, $timeout = 900 );

    if(is_file($tmpfile)){
        copy( $tmpfile, $permfile );
        unlink( $tmpfile ); 
    }   
   

    write_log('auto_sync - sync_carpet_friday_event-'.$tile->manufacturer);
    $obj = new Example_Background_Processing();
    $obj->handle_all('tile_catalog', $tile->manufacturer);
   
    write_log('Sync Completed - '.$tile->manufacturer);    
}
    }         

    write_log('Sync Completed for all tile brand');   

   // compare_csv_onsite_products('tile_catalog');
    
}